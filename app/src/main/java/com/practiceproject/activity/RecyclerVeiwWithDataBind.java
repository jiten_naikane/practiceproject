package com.practiceproject.activity;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.view.View;

import com.practiceproject.R;
import com.practiceproject.adapter.RecycletDataBindingAdapter;
import com.practiceproject.databinding.ActivityRecyclerVeiwWithDataBindBinding;

public class RecyclerVeiwWithDataBind extends AppCompatActivity {

    RecycletDataBindingAdapter recycletDataBindingAdapter;
    ActivityRecyclerVeiwWithDataBindBinding dataBinding;

    String  Names[]={"Instagram","LinkedIn","Android","Facebok","Skype","Twitter"};
    int   Images[]={R.drawable.instagram,R.drawable.linkedin,R.drawable.android,R.drawable.facebook,R.drawable.skype,R.drawable.twitter};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBinding= DataBindingUtil.setContentView(this,R.layout.activity_recycler_veiw_with_data_bind);


         recycletDataBindingAdapter=new RecycletDataBindingAdapter(RecyclerVeiwWithDataBind.this,Names,Images);
         dataBinding.recyclerView.setAdapter(recycletDataBindingAdapter);





    }

}
