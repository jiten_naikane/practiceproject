package com.practiceproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.practiceproject.MainActivity;
import com.practiceproject.R;
import com.practiceproject.activity.RecyclerVeiwWithDataBind;
import com.practiceproject.databinding.RowItemBinding;

public class RecycletDataBindingAdapter extends RecyclerView.Adapter<RecycletDataBindingAdapter.MyHolder> {

      Context mCtxt;
      String [] names;
      int [] images;

    int resource;

    public RecycletDataBindingAdapter(RecyclerVeiwWithDataBind activity, String[] names, int[] images) {
        this.mCtxt = activity;
        this.names = names;
        this.images = images;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //  View view =LayoutInflater.from(mCtxt).inflate(R.layout.row_item,parent,false);
        //return new MyHolder(view);

        RowItemBinding rowItemBinding = DataBindingUtil.inflate(LayoutInflater.from(mCtxt),R.layout.row_item,parent,false);
        return new MyHolder(rowItemBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position)
    {
     holder.binding.setForName(names[position]);
     holder.binding.setForIamge(images[position]);

    }

    @Override
    public int getItemCount() {
        return names.length;
    }


    public class MyHolder extends  RecyclerView.ViewHolder
    {
        RowItemBinding binding;
        private ImageView iv;
        private TextView tvText;
        public MyHolder(RowItemBinding rowBind)
        {
            super(rowBind.getRoot());
            this.binding=rowBind;
        }
    }


}
